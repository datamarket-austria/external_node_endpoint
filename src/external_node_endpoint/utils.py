import datetime
import time
import os

from external_node_endpoint.configuration import load_config


DT_ISO_FORMAT_Z = '%Y-%m-%dT%H:%M:%SZ'
DT_ISO_FORMAT = '%Y-%m-%dT%H:%M:%S'
DT_ISO_DATE = '%Y-%m-%d'
TS_FORMAT = '%Y-%m-%d %H:%M:%S'


def cur_time(fmt=DT_ISO_FORMAT_Z):
    return datetime.datetime.fromtimestamp(time.time()).strftime(fmt)


def change_datetime_str_fmt(date, in_fmt, out_fmt):
    date_time = datetime.datetime.strptime(date, in_fmt)
    return date_time.strftime(out_fmt)


def get_base_uri():
    load_config(keys=['DMA_EXTERNAL_NODE_HOST', 'DMA_EXTERNAL_NODE_PORT', 'DMA_EXTERNAL_NODE_DOMAIN'])
    if os.environ['DMA_EXTERNAL_NODE_DOMAIN']:
        return os.environ['DMA_EXTERNAL_NODE_DOMAIN']
    return "http://%s:%s" % (os.environ['DMA_EXTERNAL_NODE_HOST'], os.environ['DMA_EXTERNAL_NODE_PORT'])
