import os
from datetime import date
from owslib.csw import CatalogueServiceWeb
from owslib.fes import PropertyIsEqualTo, PropertyIsGreaterThanOrEqualTo, PropertyIsLessThanOrEqualTo
import xml.etree.ElementTree as ET

from external_node_endpoint.utils import change_datetime_str_fmt, DT_ISO_FORMAT_Z, DT_ISO_DATE
from external_node_endpoint.configuration import load_config


def build_constraints(identifier: str=None, parentidentifier: str=None, start: date=None, end: date=None) -> list:
    """
    Builds csw constraints depending on the given parameters.

    :param identifier: [str] record's identifier to add to constraints
    :param parentidentifier: [str] record's parentidentifier to add to constraints
    :param start: [date] earliest modification date (optional)
    :param end: [date] latest modification date (optional)
    :return: list of constraints (AND connected)
    """

    constraints = []
    if identifier is not None:
        constraints.append(PropertyIsEqualTo('apiso:Identifier', identifier))
    if parentidentifier is not None:
        constraints.append(PropertyIsEqualTo('apiso:ParentIdentifier', parentidentifier))
    if start is not None:
        start = date.strftime(start, DT_ISO_DATE)
        constraints.append(PropertyIsGreaterThanOrEqualTo('apiso:Modified', start))
    if end is not None:
        end = date.strftime(end, DT_ISO_DATE)
        constraints.append(PropertyIsLessThanOrEqualTo('apiso:Modified', end))

    if len(constraints) > 1:
        constraints = [constraints]

    return constraints


def build_resource_list(cur_logger, start: date=None, end: date=None, conf_path: str=None) -> (dict, int):
    """
    Get list of resource dicts from start and end date and data source.

    :param start: [date] earliest modification date (optional)
    :param end: [date] latest modification date (optional)
    :param conf_path: [str] path to config file (for needed parameters from config file)
    :param cur_logger: logger
    :return: list of resources and number of found records
    """
    load_config(conf_path)

    csw = CatalogueServiceWeb(os.environ['CSW_URL'])
    # Iterate over copernicus products and check for each product separately
    # a && b && (c || d) is not possible -> a && b && c || a && b && d
    copernicus_only = True  # whether only copernicus records should be taken into account, need to be changed in code!!
    if copernicus_only:
        resources_all = []
        num_of_records = 0
        products = get_products_list()
        for product in products:
            constraints = build_constraints(parentidentifier=product, start=start, end=end)
            cur_resources, cur_num = get_resource_list(csw=csw, constraints=constraints, cur_logger=cur_logger)
            if cur_num > 0:
                resources_all.extend(cur_resources)
                num_of_records += cur_num
        return resources_all, num_of_records

    constraints = build_constraints(start=start, end=end)
    return get_resource_list(csw=csw, constraints=constraints, cur_logger=cur_logger)


def get_resource_list(csw, constraints: list, cur_logger) -> (dict, int):
    """
    Get list of resource dicts from csw and constraints.

    :param csw: owslib.CatalogueServiceWeb
    :param constraints: list of constraints
    :param cur_logger: logger
    :return: list of resources and number of found records
    """
    csw.getrecords2(constraints=constraints)
    num_of_records = csw.results['matches']
    cur_logger.debug('number of records %d' % num_of_records)

    if num_of_records == 0:
        return None, 0

    resources = []
    done_records = 0
    max_records = 1000
    change_type = 'created'  # TODO do we have anything else?
    while done_records < num_of_records:
        csw.getrecords2(constraints=constraints, startposition=done_records+1, maxrecords=max_records)
        for i in csw.records:
            record = csw.records[i]
            identifier = record.identifier
            last_change = change_datetime_str_fmt(record.modified, in_fmt=DT_ISO_DATE, out_fmt=DT_ISO_FORMAT_Z)
            resources.append({"identifier": identifier, "last_change": last_change, "change_type": change_type})
            done_records += 1
        cur_logger.debug('number of done records %d' % done_records)
    return resources, num_of_records


def get_raw_metadata(identifier: str, conf_path: str, cur_logger) -> str:
    """
    Get raw xml metadata.

    :param identifier: [str] identifier for which metadata should be retrieved
    :param conf_path: [str] path to config file (for needed parameters from config file)
    :param cur_logger: [str] logger
    :return: [str] string representation of MD_Metadata element
    """
    load_config(path=conf_path, keys=['CSW_URL', 'CSW_OUTPUT_SCHEMA', 'CSW_OUTPUT_FORMAT'])

    constraints = build_constraints(identifier=identifier)
    csw = CatalogueServiceWeb(os.environ['CSW_URL'])
    csw.getrecords2(constraints=constraints, esn='full', outputschema=os.environ['CSW_OUTPUT_SCHEMA'],
                    format=os.environ['CSW_OUTPUT_FORMAT'])
    # TODO handle csw error

    metadata = get_md_metadata(csw.response)
    cur_logger.debug('Raw metadata: \n%s' % str(metadata))
    cur_logger.info('Retrieved raw metadata for identifier %s' % identifier)
    return metadata


def get_md_metadata(xml: bytes) -> str:
    """
    Get the MD_Metadata element from xml metadata.

    :param xml: [bytes] xml byte array
    :return: [str] string representation of MD_Metadata element
    """
    root = ET.XML(xml)
    return ET.tostring(root[1][0], encoding='utf-8').decode('utf-8')


def get_products_list() -> list:
    """
    Returns the list of products (copernicus)
    """

    return [
        's1a_csar_grdh_ew',
        's1a_csar_grdh_iw',
        's1a_csar_grdm_ew',
        's1a_csar_slc__ew',
        's1a_csar_slc__iw',
        's1b_csar_grdh_ew',
        's1b_csar_grdh_iw',
        's1b_csar_grdm_ew',
        's1b_csar_slc__iw',
        's2a_prd_msil1c',
        's2b_prd_msil1c',
        's3a_ol_1_efr___',
        's3a_ol_1_err___',
        's3a_sl_1_rbt___'
    ]
