import logging
from datetime import datetime, timedelta
from flask import Flask, render_template, make_response, Response, jsonify, url_for

from assets_db.query import get_info_from_asset_id
from external_node_endpoint.utils import DT_ISO_FORMAT_Z, get_base_uri
from external_node_endpoint.csw import get_raw_metadata
from external_node_endpoint.mapper import get_mapped_xml
from external_node_endpoint.assets import add_missing_dma_placeholders, sync_dates_for_changelist_index, \
    get_resources_for_date
from external_node_endpoint.configuration import get_config_path

BASE_URL = '/'
RESOURCE_SET = 'copernicus'
SOURCE_DESCRIPTION = 'resourcesync_description.xml'
CAPABILITY_LIST = 'capabilitylist.xml'
CHANGELIST = 'changelist.xml'
RESOURCES = 'resources'

app = Flask(__name__)

# Setup logging for Gunicorn
if __name__ != '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)


@app.route(BASE_URL)
@app.route(BASE_URL + SOURCE_DESCRIPTION)
def source_description():
    """
    Generate ResourceSync Source Description.
    :return: ResourceSync Source Description xml
    """
    app.logger.info('Source Description accessed')
    context = {
        'base_uri': get_base_uri(),
        'capabilitylist_uri': url_for('capability_list')
    }

    template = render_template('source_description.xml', **context)
    response = make_response((template, {'Content-Type': 'application/xml;'}))
    return response


@app.route(BASE_URL + CAPABILITY_LIST)
def capability_list():
    """
    Generate ResourceSync Capability List.
    :return: ResourceSync Capability list xml
    """
    app.logger.info('Capability List accessed')
    context = {
        'base_uri': get_base_uri(),
        'source_description_uri': url_for('source_description'),
        'changelist_index_uri': url_for('changelist_index')
    }
    template = render_template('capabilitylist.xml', **context)
    response = make_response((template, {'Content-Type': 'application/xml;'}))
    return response


@app.route(BASE_URL + RESOURCE_SET + '/' + CHANGELIST)
def changelist_index():
    """
    Generate ResourceSync Resource List Index.
    :return: ResourceSync changelist index xml
    """
    app.logger.info('Change List Index accessed')

    first_date, changelists = sync_dates_for_changelist_index(conf_path=get_config_path())
    app.logger.info('%d change lists available' % len(changelists))
    for c in changelists:
        c['uri'] = url_for('changelist', req_date=c['prefix'])

    context = {
        'base_uri': get_base_uri(),
        'capabilitylist_uri': url_for('capability_list'),
        'first_date': first_date,
        'changelists': changelists
    }
    template = render_template('changelist_index.xml', **context)
    response = make_response((template, {'Content-Type': 'application/xml;'}))
    return response


@app.route(BASE_URL + RESOURCE_SET + '/<req_date>-' + CHANGELIST)
def changelist(req_date):
    """
    Generates ResourceSync Change List depending on requested date (%Y%m%d).
    :param req_date: [str] date to get changelist for
    :return: ResourceSync changelist xml
    """
    app.logger.info('Change List %s accessed' % req_date)

    req_date = datetime.strptime(req_date, '%Y%m%d')
    resources = get_resources_for_date(req_date, conf_path=get_config_path())
    app.logger.info('%d resources are available for this date' % len(resources))
    for r in resources:
        r['uri'] = url_for('resource', asset_id=r['asset_id'])
    context = {
        'base_uri': get_base_uri(),
        'capabilitylist_uri': url_for('capability_list'),
        'changelist_index_uri': url_for('changelist_index'),
        'start': req_date.strftime(DT_ISO_FORMAT_Z),
        'end': (req_date + timedelta(days=1)).strftime(DT_ISO_FORMAT_Z),
        'resources': resources
    }

    template = render_template('changelist.xml', **context)
    response = make_response((template, {'Content-Type': 'application/xml;'}))
    return response


@app.route(BASE_URL + RESOURCE_SET + '/' + RESOURCES + '/<asset_id>')
def resource(asset_id):
    """
    Perform on-the-fly mapping from eodc to DMA
    :param asset_id: eodc identifier for which the mapping should be performed
    :return: DMA compliant xml (to be inserted into the DMA metadata catalogue)
    """
    app.logger.info('Resource %s accessed' % asset_id)

    cfg_path = get_config_path()
    obj = get_info_from_asset_id(asset_id=asset_id, conf_path=cfg_path)
    if obj is None:
        return jsonify({'ERROR': 'Given Asset-ID does not exist!'})
    identifier, dataset_puid = obj

    raw_metadata = get_raw_metadata(identifier=identifier, conf_path=cfg_path, cur_logger=app.logger)

    xml_metadata, mapping_worked = get_mapped_xml(identifier=identifier, raw_metadata=raw_metadata, conf_path=cfg_path,
                                                  cur_logger=app.logger)
    if not mapping_worked:
        return jsonify(xml_metadata)

    metadata = add_missing_dma_placeholders(xml_metadata=xml_metadata, dataset_puid=dataset_puid,
                                            asset_id=asset_id, cur_logger=app.logger)

    return Response(metadata, mimetype='text/xml')


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
