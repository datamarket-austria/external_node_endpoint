import os
import uuid
import requests
import rdflib

from external_node_endpoint.configuration import load_config


def get_mapped_xml(identifier, raw_metadata: str, conf_path: str, cur_logger):
    """
    Perform mapping and transformation to dcat xml.

    :param identifier: [str] metadata's identifier which is mapped
    :param raw_metadata: [str] metadata which should be mapped
    :param conf_path: [str] pyth to config file
    :param cur_logger: logger
    :return:
    """
    load_config(path=conf_path, keys=['MAPPING_NAME', 'MAPPING_RAW_FORMAT', 'MAPPING_URL'])

    mapped_metadata, mapper_status = call_mapper(raw_metadata=raw_metadata, mapping_name=os.environ['MAPPING_NAME'],
                                                 mapping_raw_format=os.environ['MAPPING_RAW_FORMAT'],
                                                 mapping_url=os.environ['MAPPING_URL'], cur_logger=cur_logger)

    mapping_error = check_for_mapping_error(mapped_metadata, identifier, os.environ['MAPPING_NAME'], cur_logger)
    if mapping_error is not None:
        context = {
            'ERROR': 'MAPPING FAILED',
            'identifier': identifier,
            'raw_metadata': raw_metadata,
            'raw_format': os.environ['MAPPING_RAW_FORMAT'],
            'mapping_name': os.environ['MAPPING_NAME'],
            'mapping_url': os.environ['MAPPING_URL'],
            'mapping_error': mapping_error
        }
        return context, False
    cur_logger.info('Performed mapping. Mapper status code: %d' % mapper_status)

    xml_metadata = ttl2xml(mapped_metadata)
    cur_logger.info('Performed ttl2xml conversion.')

    return xml_metadata, True


def call_mapper(raw_metadata: str, mapping_name: str, mapping_raw_format: str, mapping_url: str, cur_logger) -> (str, int):
    """
    Call metadata mapper for raw_metadata.

    :param raw_metadata: [str] metadata which should be mapped
    :param mapping_name: [str] name of mapping which should be used to map raw_metadata
    :param mapping_raw_format: [str] format of raw_metadata (xml or json)
    :param mapping_url: [str] url where mapper is running
    :param cur_logger: logger
    :return: mapped metadata [str] and status code [int]
    """
    random_id = str(uuid.uuid4())
    # TODO add env variables for DMA_PLACEHOLDER -> see mapper dockerfile
    url = "%s/%s?format=%s&temp_if=%s" % (mapping_url, mapping_name, mapping_raw_format, random_id)
    r = requests.post(url, raw_metadata)

    cur_logger.debug("\nurl:  " + url + "\n\n")
    cur_logger.debug("status mapper:  " + str(r.status_code) + " \t\t " + str(r.text))

    return r.text, r.status_code


def ttl2xml(metadata_graph: str) -> bytes:
    """
    Transform rdf file to xml.

    :param metadata_graph: [str] rdf metadata graph
    :return: [bytes] transformed xml
    """
    graph = rdflib.Graph()
    graph.parse(data=metadata_graph, format="n3")
    return graph.serialize(format='xml')


def check_for_mapping_error(mapped_metadata: str, identifier: str, mapping_name: str, cur_logger) -> str:
    """
    Check if mapping produced an error.

    :param mapped_metadata: [str] mapper response
    :param identifier: [str] metadata's identifier which was mapped
    :param mapping_name: [str] Mapping name
    :param cur_logger: logger
    :return: [str] mapping error or None (if no error occurred)
    """
    mapping_error = None
    if mapped_metadata == "Mapping not found":
        mapping_error = "404 Mapping not found"
        cur_logger.info("FAILED MAPPING " + mapping_name + " identifier:" + identifier)

    elif mapped_metadata == "Input Not compliant with mapping file":
        mapping_error = "400 Input Not compliant with mapping file"
        cur_logger.info("FAILED MAPPING " + mapping_name + " identifier:" + identifier)

    elif mapped_metadata is None or len(mapped_metadata) < 5:
        mapping_error = "500 Error from mapper"
        cur_logger.info("FAILED MAPPING " + mapping_name + " identifier:" + identifier)

    return mapping_error
