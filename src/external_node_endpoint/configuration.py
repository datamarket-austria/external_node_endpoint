import os
import configparser


base_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
settings_path = os.path.join(base_dir, 'settings.cfg')


def load_config(path: str=None, keys: list=None):

    mapping = {
        # server
        'DMA_EXTERNAL_NODE_DEFAULT_START': ['external_node_endpoint', 'start_time_default'],
        'DMA_EXTERNAL_NODE_HOST': ['server', 'service_host'],
        'DMA_EXTERNAL_NODE_PORT': ['server', 'service_port'],
        'DMA_EXTERNAL_NODE_DOMAIN': ['server', 'service_domain'],

        # csw
        'CSW_URL': ['csw', 'url'],
        'CSW_OUTPUT_SCHEMA': ['csw', 'output_schema'],
        'CSW_OUTPUT_FORMAT': ['csw', 'output_format'],

        # mapper
        'MAPPING_URL': ['mapper', 'url'],
        'MAPPING_NAME': ['mapper', 'mapping_name'],
        'MAPPING_RAW_FORMAT': ['mapper', 'raw_format'],

        # db
        'DMA_ASSETS_DB_URL': ['assets_db', 'url']
    }

    if keys is None:
        keys = mapping.keys()

    # Only load not set keys
    missing_keys = []
    for key in keys:
        if os.environ.get(key) is None:
            missing_keys.append(key)
    if len(missing_keys) == 0:
        return

    path = get_config_path(path)
    config = configparser.ConfigParser()
    config.read(path)

    for key in missing_keys:
        val = mapping[key]
        os.environ[key] = config.get(val[0], val[1])


def get_config_path(path: str=None):
    if path is not None and os.path.isfile(path):
        return path
    if os.environ.get('DMA_EXTERNAL_NODE_CONFIG_PATH') is not None and\
            os.path.isfile(os.environ['DMA_EXTERNAL_NODE_CONFIG_PATH']):
        return os.environ['DMA_EXTERNAL_NODE_CONFIG_PATH']

    folder = 'dma'
    config_filename = 'external_node_endpoint.cfg'
    etc_cfg = '/etc/%s/%s' % (folder, config_filename)
    home_cfg = os.path.expanduser('~/.%s/%s' % (folder, config_filename))

    if os.path.isfile(home_cfg):
        return home_cfg
    if os.path.isfile(etc_cfg):
        return etc_cfg
