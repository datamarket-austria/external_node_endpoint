from datetime import timedelta, date

from assets_db.query import get_synced_dates, get_resources_info
from external_node_endpoint.utils import DT_ISO_FORMAT_Z


def add_missing_dma_placeholders(xml_metadata: bytes, dataset_puid: str, asset_id: str, cur_logger) -> (str, bool):
    """
    Set blockchain related DMA_PLACEHOLDERs.

    :param xml_metadata: [bytes] metadata where placeholder should be replaced
    :param dataset_puid: [str] records datasetPUID
    :param asset_id: [str] records Asset-ID
    :param cur_logger: logger
    :return: metadata and True if DISTRIBUTION_DEFAULT_ID could be set, False if not
    """
    metadata = xml_metadata.decode('utf-8')
    metadata = metadata.replace('DMA_PLACEHOLDER_DATASET_ID', dataset_puid)
    metadata = metadata.replace('DMA_PLACEHOLDER_DISTRIBUTION_DEFAULT_ID', asset_id)
    cur_logger.info('Distribution ID added.')
    return metadata


def sync_dates_for_changelist_index(conf_path: str=None):
    """
    Create list of dicts containing changelist index info.
    :param conf_path: [str] path to config file (asset database url needs to be included)
    :return: [tuple] earliest date and list of dicts containing changelist index context
    """
    checked_dates = get_synced_dates(conf_path=conf_path)
    # If database is empty (no date is done) no changelist can be provided
    if not checked_dates:
        return '', []
    checked_dates.sort()
    first_date = checked_dates[0].strftime(format=DT_ISO_FORMAT_Z)

    changelists = []
    for checked_date in checked_dates:
        changelists.append({
            'prefix': checked_date.strftime(format='%Y%m%d'),
            'start': checked_date.strftime(format=DT_ISO_FORMAT_Z),
            'end': (checked_date + timedelta(days=1)).strftime(format=DT_ISO_FORMAT_Z)
        })
    return first_date, changelists


def get_resources_for_date(req_date: date, conf_path: str=None):
    """
    Create list of dicts containing resource information for changelist.
    :param req_date: [date] requested date to create information for
    :param conf_path: [str] path to config file (asset database url needs to be included)
    :return: [list] list of dicts
    """
    db_resources = get_resources_info(req_date, conf_path=conf_path)

    resources = []
    for res in db_resources:
        _, last_mod, change_type, asset_id = res
        resources.append({
            'asset_id': asset_id,
            'last_change': last_mod.strftime(format=DT_ISO_FORMAT_Z),
            'change_type': change_type
        })
    return resources
