FROM ubuntu:16.04

RUN apt-get update && apt-get install -y libssl-dev/xenial openssl/xenial
RUN apt-get update && apt-get install python3 python3-setuptools build-essential python3-dev python3-pip git --force-yes -y

# Prepare ssh deploy key to install package from gitlab.com
ARG id_rsa_path=id_rsa_assets_db
RUN mkdir /root/.ssh
COPY $id_rsa_path /root/.ssh/id_rsa
RUN touch /root/.ssh/known_hosts
RUN ssh-keyscan gitlab.com >> /root/.ssh/known_hosts

# Install python package
RUN pip3 install gunicorn setuptools==40.8.0
WORKDIR /external_node_endpoint
COPY . .
RUN python3 setup.py install

# Remove deploy key file
RUN rm /root/.ssh/id_rsa
# Remove generate folders which are not needed
RUN rm -rf dist build .eggs

ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8

# Create AssetsDB
RUN mkdir db
RUN assets_db -u sqlite:///db/assets.db create_db

# Prepare for config - has to be mounted!!
RUN mkdir /root/.dma
RUN touch /root/.dma/external_node_endpoint.cfg

EXPOSE 5000
WORKDIR /external_node_endpoint/src/external_node_endpoint
CMD ["gunicorn", "-b", "0.0.0.0:5000", "-w", "2", "wsgi:app"]
