# external_node_endpoint

Simple flask server implementing [ResourceSync] protocol (providing
changelist and resource xml files for synchronisation with DMA).


## Description

This package should be used as a template how a DMA changelist and resource xml files could be provided. It
should be used by external nodes which already have a metadata archive up and running and want to insert their metadata
automatically into the DMA archive without duplicating their metadata. This workflow generates needed DMA DCAT xml
files on-the-fly when requested.

The only part of this workflow which is not done on-the-fly is getting DMA (blockchain) Asset-IDs and matching them with
external node records. This is done beforehand and this record - Asset-ID matching is stored in a database
[assetsDB]. To see an example how this database could be filled see [assetsRetrieval].

The section *Changes needed to be used with different external node* explains which parts need to be modified to use a
different metadata provider.


## Installation

### Configuration
A simple config file is used to configure the package. ``settings_sample.cfg`` shows the accepted structure and parameters.
In the following the single parameters are described in more detail:

- [server]:
    - service_host: host where the service is running (e.g.: localhost or 0.0.0.0)
    - service_port: port where the service is running (e.g.: 5000)
    - service_domain: complete domain, if set service_host and service_port are ignored (e.g. https://node.dma.at)
- [assets_db]
    - url: database url to AssetsDB. (Location of sqlite file)
- [mapper]
    A Metadata mapper service has to be up and running to map raw metadata to DCAT xml. Also the used mapping needs to
    be in Metadata Mapper, therefore it first needs to be inserted. See [Metadata Mapper]
    how this is done.
    - url: Metadata Mapper url (like: http://host:port/RMLMapper/mapper/mappingLibrary)
    - mapping_name: (Metadata Mapper parameter) name of the mapping to use
    - raw_format: (Metadata Mapper parameter) format of the raw metadata (xml, json)
- [external_node_endpoint]
    - start_time_default: Add the date of the first record. Used in the changelist description if no start date is
    supplied.
- [csw]
    If you have a CSW server to provide metadata add the configuration here, otherwise delete this section and
    add additional parameters needed to access your metadata. Keep in mind to also change configuration.py for need
    parameters.
    - url: url to CSW server
    - output_schema: raw metadata output schema
    - output_format: raw metadata output format (xml, json)


### Virtualenv
This section explains how this package can be installed into a virtual environment
1. Clone this repository (using ssh in this case) and navigate into the folder
    ```bash
    git clone git@gitlab.com:datamarket/external_node_endpoint.git
    cd /path/to/external_node_endpoint
    ```
1. Create/Modify settings file
    ```bash
    mkdir ~/.dma
    cp settings_sample.cfg ~/.dma/external_node_endpoint.cfg
    ```
    Inside the new external_node_endpoint.cfg file several parameter need to be modified:
    - external_node_endpoint: 
        - start_time_default: set date of first record in your data archive (needs to be in %Y-%m-%dT%H:%M:%SZ format)
    - server: 
        - service_host and service_port: add where this service is running
    - mapper:
        - url: add url where the your instance of the [Metadata Mapper]
        is running
        - mapping_name: add name of mapping which should be used (This mapping has to be added to your metadata mapper!)
        - raw_format: add the format of your raw metadata (xml or json)
    - assets_db:
        - url: add url where your [assetsDB] is located

1. Recommended: Create a virtual environment and activate it, tested with Python 3.6
    ```bash
    virtualenv external_node_endpoint -p python3.6
    source external_node_endpoint/bin/activate
    ```
1. Install package (Make sure you are inside the assets_db folder)
    ```bash
    cd /path/to/external_node_endpoint
    python setup.py install
    ```
    Replace ``install`` with ``develop`` if you plan to modify code.

### Docker
The package also includes a Dockerfile to simplify the setup.

#### Manual setup:
1. Create assetsDB volume:
    ```bash
    docker volume create assets-db-data
    ```
1. Build docker image:
    ```bash
    docker build -t dma/external_node_endpoint .
    ```
    Note: A ssh deploy key is used to install the [assetsDB] package. The argument
   ``id_rsa_path`` can be set to pass the path to this deploy key (add ``--build-arg id_rsa_path=/path/to/id_rsa`` to
    build command). If not set the deploy key is assumed to be in the same folder as the Dockerfile.
1. Prepare config:
    Copy the settings_sample.cfg:
    ```bash
    cp settings_sample.cfg external_node_endpoint.cfg
    ```
    Open the copied file and set parameters (see *Configuration* section). This file will be mounted into
    the container in the next step, therefore the image does not need to be rebuild if the config changes.
    [Gunicorn] is used as WSGI HTTP Server. By default 2 workers are used. Check the Dockerfile to adopt this.
1. Run container:
    ```bash
    docker run --name external_node_endpoint \
    -v assets-db-db:/external_node_endpoint/db \
    -v ./external_node_endpoint.cfg:/root/.dma/external_node_endpoint.cfg \
    dma/external_node_endpoint
    ```
    The service will run under http://0.0.0.0:5000

#### docker-compose
Take a look at [externalNodeSetup] for an example of a docker-compose
file which deploys all needed components to become a dynamic data provider.

## Usage

### Run server (Virtualenv Setup)
1. Activate virtual environment
    ```bash
    cd /path/to/external_node_endpoint
    source external_node_endpoint/bin/activate
    ```
1. Set flask app and run flask
    ```bash
    export FLASK_APP=src/external_node_endpoint/server.py
    flask run --host=0.0.0.0
    ```
    ``--host=0.0.0.0`` is needed to reach the server from outside the local machine
    For developing add ``export FLASK_ENV=development``, then the server is automatically reloaded when code changes

### Query server
This is an implementation of the [ResourceSync] protocol therefore
all needed routes are provided. The following are examples using eodc data. As described in the ResourceSync protocol
all files are linked therefore knowing the Source Description uri one can explore all provided data.

- Source Description: ``<service_host>:<service_port>/`` or ``<service_host>:<service_port>/resourcesync_description.xml``
- Capability List: ``<service_host>:<service_port>/capabilitylist.xml``
- Changelist Index: ``<service_host>:<service_port>/copernicus/changelist.xml``
- Changelist: ``<service_host>:<service_port>/copernicus/<%Y%m%d>-changelist.xml`` e.g. 20190130-changelist.xml
- Resource: ``<service_host>:<service_port>/copernicus/resources/<assetID>``

## Changes needed to be used with different external node
Source Description, Capability List, Changelist Index and Changelist are complete created from the config file and
AssetsDB, therefore if AssetsDB is filled and a proper config file is provided nothing has to be modified there.

### Resource
To actually map an external metadata record, this record has to be retrieved first that is why changes are needed to
properly generate Resources. Inside server.py resource() the function get_raw_metadata() is called. This function has to
be reimplemented.

The input arguments are:
- ``identifier``: identifier of the record for which raw metadata should be returned
- ``conf_path``: path to config file (can be used to pass additional needed parameters)
- ``cur_logger``: python logger

This function has to return metadata of the given identifier in the metadata format of the external node. Again eodc
uses a OGC compliant CSW server to provide metadata, so take a look into the csw.py file if you are also using such
a server.

Make sure after reimplementing this function that your function is imported into server.py!


## Note

This project has been set up using PyScaffold 3.1. For details and usage
information on PyScaffold see https://pyscaffold.org/.

[externalNodeSetup]: https://gitlab.com/datamarket-austria/external_node_setup
[assetsDB]: https://gitlab.com/datamarket-austria/assets_db
[assetsRetrieval]: https://gitlab.com/datamarket-austria/assets_retrieval
[Metadata Mapper]: https://gitlab.com/datamarket/dma-simple-RML-mapper
[ResourceSync]: http://www.openarchives.org/rs/1.1/resourcesync
[Gunicorn]: https://gunicorn.org/
